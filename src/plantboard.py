class PlantBoard:
    def __init__(self):
        self.sensors = []
        self.used_pin = []
    
    def add_sensor(self, sensor):
        if(sensor.pin_reader.pin not in self.used_pin):
            self.used_pin.append(sensor.pin_reader.pin)
            self.sensors.append(sensor)
        else:
            print("Can't create a sensor with an in-use pin")

    def json(self):
        dict = {}
        for sensor in self.sensors:
            dict.update(sensor.json())
        return dict
