import logging
import time

from src.caller import Caller
from src.plantboard import PlantBoard
from src.readers.pin_reader import PinReader
from src.readers.pin_simulated import PinSimulated
from src.sensor import Sensor

plantboard = PlantBoard()

caller = Caller("http://localhost:4000/api/donnees")


id_token = ""
token = ""

while True:
    id_token = input("Input your IOT idToken\n")
    if(len(id_token) == 24):
        break
    else:
        print("L'id token est une suite de 24 caractères")

while True:
    token = input("Input your token\n")
    if(len(token) == 32):
        break
    else:
        print("Le token est une suite de 32 caractères")

file = open("settings.txt", "r")
lines = file.readlines()
for line in lines:
    splitted = line.split(",")
    id_variable = splitted[0]
    pin = int(splitted[1])
    name = splitted[2]
    #plantboard.add_sensor(Sensor(id_variable, PinReader(pin), name))
    plantboard.add_sensor(Sensor(id_variable, PinSimulated(pin), name))
file.close()

input_create_new_sensors = input("Create new sensors ? y/N").lower()

if(plantboard.sensors == [] or input_create_new_sensors == "y"):
    print("\nBeginning new sensors creations\n")
    while(True):
        id_variable = input("\nInput the variable ID from the application\n")
        pin = int(input("\nInput pin number\n"))
        name = input("\nInput the name of the sensor\n")

        plantboard.add_sensor(Sensor(id_variable, PinSimulated(pin), name))
        #plantboard.add_sensor(Sensor(id_variable, PinReader(pin), name))

        input_quit = input(
            "\nType 'q' to quit or anything else to create another sensor\n").lower()
        if(input_quit == 'q'):
            file = open("settings.txt", "w")
            for sensor in plantboard.sensors:
                line = str(sensor.id_variable) + "," + \
                    str(sensor.pin_reader.pin) + "," + str(sensor.name) + "\n"
                file.write(line)
            file.close()
            break

while(True):
    print("\n Beginning of the loop")
    logging.debug("Beginning of the loop")
    for sensor in plantboard.sensors:
        body = {"idToken": id_token, "token": token}
        body.update(sensor.json())
        caller.post(body)

    print("\n the loop has finished")
    time.sleep(1)
