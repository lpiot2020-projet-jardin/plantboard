class Sensor:
    def __init__(self, id_variable, pin_reader, name):
        self.id_variable = id_variable
        self.pin_reader = pin_reader
        self.name = name
    
    def read(self):
        return self.pin_reader.readPin()
    
    def json(self):
        value = self.read()
        return {"idVariable": self.id_variable, "value": value}

