import requests

class Caller():
    def __init__(self, url):
        self.url = url

    def post(self, data):
        return requests.post(self.url, json=data)