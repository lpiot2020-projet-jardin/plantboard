import random

class PinSimulated:
    def __init__(self, pin_number):
        self.pin = pin_number

    def readPin(self):
        return random.randrange(1, 100)
